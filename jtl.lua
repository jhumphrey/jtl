local jtl = { 
	["bracket"] = {
		["start"] = "|%%",
		["stop"] = "%%|"
	},
	["getval"] = function(values,what)
-- get percent at the start?
		if string.sub(what,1,1) == "%" then
			require "support.urlknife"
			local url2encode = jtl.getval(values,what:sub(2,-1))
			if url2encode then
return (percent_enc(url2encode))
			end
			return ""
-- does this value need to be converted?
		elseif string.sub(what,1,1) == "(" then
			-- get all the way to the closing bracket
			local closingstart = string.find(what, ")")
			if closingstart then
				typestr = string.sub(what, 2, closingstart-1)
				valstring = string.sub(what, closingstart+1, -1)
				return jtl.convert(jtl.getval(values,valstring),typestr)
			else
				print("WARNING, COULD NOT FIND CLOSING PARENTHESIS")
			end
		end
--print("DEBUG: what is "..what)
		if not values then
			return false
		end
		-- split this up according to dots
		if type(what) == "number" then
			return values[number]
		end
		local slash = what:find("/")
		if slash then 
			return jtl.getval(jtl.getval(values,what:sub(1,slash-1)),jtl.getval(values,what:sub(slash+1,-1)) or "nil")
		else
			local dot = what:find("%.")
			if dot then
				return jtl.getval(jtl.getval(values,what:sub(1,dot-1)),what:sub(dot+1,-1) or "nil")
			else
				local hash = what:find("#")
				if hash then
					return jtl.getval(jtl.getval(values,what:sub(1,hash-1)),tonumber(what:sub(hash+1,-1)))
				end
			end
			if what == "" then
				return values --TODO: Is this a security vulnerability?
			end
			return values[what]
		end
	end,
	["fill"] = function(str, values)
		local new_str = [[]]
		local block = [[]]
		local blockname = ""
		local ts = 0
		local bs = 0
		local inblock = false
		local sa, sb =str:find(jtl.bracket.start)
		local ea, eb = str:find(jtl.bracket.stop)
		while eb do
			-- check to see if we are a block or not.
			if inblock then
				block = block..str:sub(ts+1,sa-1)
				-- check for end block
				if str:sub(sb+1,sb+1+#blockname) == ":"..blockname then
					new_str = new_str..jtl.handle_block(block,values)
					inblock = false
				else
					block = block..str:sub(sa,eb)
				end
			else
				new_str = new_str..str:sub(ts+1,sa-1)
				if str:sub(sb+1,sb+1) == ":" then
					blockname = str:sub(sb+2,str:find(" ",sb)-1)
					inblock = true
					block = str:sub(sb+3+#blockname,eb)
				else
					-- handle the variable
					-- new_str = new_str..tostring(values[str:sub(sb+1,ea-1)])
					new_str = new_str..tostring(jtl.getval(values,str:sub(sb+1,ea-1)))
				end
			end
			ts = eb
			sa, sb = str:find(jtl.bracket.start,eb)
			ea, eb = str:find(jtl.bracket.stop,sb or #str)
		end
		new_str = new_str..str:sub(ts+1,#str)
		return new_str
	end,
	["parse_cmd"] = function(str)
--print("DEBUG: str is "..str)
		-- split by whitespace and comma
		local ctab = {}
		local prevsep = 0
		local start,sep = str:find("[%s,+]")
		while sep do
			ctab[#ctab+1] = str:sub(prevsep+1,start-1)
--print("DEBUG: CTAB TOP IS \""..(ctab[#ctab] or "nil").."\"")
			prevsep = sep
--print("DEBUG: sep IS "..sep.." #STR IS "..#str)
			start,sep = str:find("[%s,]+",prevsep+1)
		end
		ctab[#ctab+1] = str:sub(prevsep+1,-1)
--print("DEBUG: CTAB TOP IS \""..(ctab[#ctab] or "nil").."\"")
		return ctab
	end,
	["handle_block"] = function(block, values)
		local result = [[]]
		-- determine blocktype, that is the first line
		-- go till the first %|
		local cmdstop = block:find(jtl.bracket.stop)
		local cmd = block:sub(1,cmdstop-1)
--[[
		-- determine the first word of cmd
		local btstop = cmd:find("%s")
		local bt = cmd:sub(1,btstop-1)
--]]
		local args = jtl.parse_cmd(cmd)
		bt = args[1]
		if bt == "pairs" then
			-- determine what we are going through in values.
--[[
			local whatstop = cmd:find("%s",btstop+1)
			local what = cmd:sub(btstop+1,whatstop-1)
			local keywordstop = cmd:find(",",whatstop)
			local kw_begin = cmd:find("[^%s]",whatstop)
			local keyword = cmd:sub(kw_begin,(keywordstop or 0)-1)
			local valwordstop = cmd:find("%s",keywordstop)
			local valword = cmd:sub(keywordstop+1,(valwordstop or 0)-1)
--]]
-- :a pairs array k,v
			local what = args[2]
			local keyword = args[3]
			local valword = args[4]
			
			if what and keyword then
				if jtl.getval(values,what) then
					for k, v in pairs(jtl.getval(values,what)) do
						values[keyword] = k
						if valword then
							values[valword] = v
						end
						result = result..jtl.fill(block:sub(cmdstop+2,-1),values)
					end
				end
			end
		elseif bt == "ipairs" then
--[[
			local whatstop = cmd:find(" ",btstop+1)
			local what = cmd:sub(btstop+1,whatstop-1)
			local keywordstop = cmd:find(",",whatstop)
			local keyword = cmd:sub(whatstop+1,(keywordstop or 0)-1)
			local valwordstop = cmd:find(" ",keywordstop)
			local valword = cmd:sub(keywordstop+1,(valwordstop or 0)-1)
--]]
			local what = args[2]
			local keyword = args[3]
			local valword = args[4]
			if what and keyword then
				--if values[what] then
				if jtl.getval(values,what) then
					for k, v in ipairs(jtl.getval(values,what)) do
						values[keyword] = k
						if valword then
							values[valword] = v
						end
						result = result..jtl.fill(block:sub(cmdstop+2,-1),values)
					end
				end
			end
		elseif bt == "ipairs" then
			-- determine what we are going through in values.
			local whatstop = cmd:find(" ",btstop+1)
			local what = cmd:sub(btstop+1,whatstop-1)
			local iwordstop = cmd:find(",",whatstop)
			local iword = cmd:sub(whatstop+1,(iwordstop or 0)-1)
			local valwordstop = cmd:find(" ",iwordstop)
			local valword = cmd:sub(iwordstop+1,(valwordstop or 0)-1)
			if what and keyword then
				if values[what] then
					for i, v in ipairs(values[what]) do
						values[iword] = i
						if valword then
							values[valword] = v
						end
						result = result..jtl.fill(block:sub(cmdstop+2,-1),values)
					end
				end
			end
		elseif bt == "opairs" then
			local what = args[2]
 			local order = args[3]
 			local keyword = args[4]
 			local valword = args[5]
			if what and order and keyword then
 				if jtl.getval(values, what) and jtl.getval(values, order) then
	 				local sourcetab = jtl.getval(values, what)
 					for i, o in ipairs(jtl.getval(values, order)) do
 						values[keyword] = o
					end
				end
			end
		elseif bt=="cmp" then
			local ZF = false
			if args[3] == "==" then
				if (jtl.getval(values,args[2]) or 0) == (jtl.getval(values,args[4]) or 1) then
					ZF = true
				end
			elseif args[3] == ">=" then
				if (tonumber(jtl.getval(values,args[2])) or 0) >= (tonumber(jtl.getval(values,args[4])) or 1) then
					ZF = true
				end
			elseif args[3] == "<=" then
				if (tonumber(jtl.getval(values,args[2])) or 1) <= (tonumber(jtl.getval(values,args[4])) or 0) then
					ZF = true
				end
			elseif args[3] == ">" then
				if (tonumber(jtl.getval(values,args[2])) or 0) > (tonumber(jtl.getval(values,args[4])) or 1) then
					ZF = true
				end
			elseif args[3] == "<" then
				if (tonumber(jtl.getval(values,args[2])) or 1) < (tonumber(jtl.getval(values,args[4])) or 0) then
					ZF = true
				end
			elseif args[3] == "!=" or args[3] == "~=" then
				if (jtl.getval(values,args[2]) or 0) ~= (jtl.getval(values,args[4]) or 0) then
					ZF = true
				end
			end 
			if ZF then
				result = jtl.fill(block:sub(cmdstop+2,-1),values)
			else
				result = ""
			end
		elseif bt=="exists" then
			local field = args[2]
			if values[field] == nil then
				return ""
			end
			result = jtl.fill(block:sub(cmdstop+2,-1),values)
		else
			print("ERROR: unsupported type '"..bt.."'")
		end
		return result
	end,
	["convert"] = function(val, typ)
		if val == nil then
			return
		end
		if typ == "date" then
			return os.date("%Y-%m-%d", tonumber(val))
		elseif typ == "date-US" then
			return os.date("%m/%d/%Y", tonumber(val))
		elseif typ == "date-UK" then
			return os.date("%d/%m/%Y", tonumber(val))
		end
	end
}

return jtl
