# JTL Johnpaul's Templating Language

Johnpaul's Templating Language

# What is this?
This is a Templating language.
It is intended for use with HTML, but it could also be used with all sorts of other things.
It basically fills a text file with values from a lua table.

# How do I use it?

first you need a table of values in your lua file.
```lua
values = {
Country = "Kyrgyzstan",
FamousPeople = {
    {
        Name = "Ильяз Андаш",
        What = "Singer",
        WhatKyrgyz = "Ырчы",
        MyFavoriteSongs = {
            ["Жонокой Жигит"]  = "https://invidious.tube/watch?v=eME3h6HUk40",
            ["Белес"] = "https://invidious.tube/watch?v=lq4gin_HKA0",
            ["Гулнара ЫССЫК КУЛЬ и ты"] = "https://invidious.tube/watch?v=3wOcoRsyNCk",
            ["Жалган Махабат"] = "https://www.super.kg/media/audio/59359"
            -- there are more of my favorites, but this is getting long
        }
    },
    {
        Name = "Айар Акылбеков",
        What = "Singer",
        WhatKyrgyz = "Ырчы",
        MyFavoriteSongs = {
            ["Ардагым"] = "https://www.super.kg/media/audio/102061",
            ["Көлдүн үстү"] = "https://www.super.kg/media/audio/152839",
            ["Жаным Алтынай"] = "https://www.super.kg/media/audio/259975",
            ["Карлыгачым"] = "https://www.super.kg/media/audio/131277",
            ["Сен менен"] = "https://www.super.kg/media/audio/191207"
            -- I am not going to list everything he ever wrote, but I like it.
        }
    },
    {
        Name = "Садыр Жапаров",
        What = "President",
        WhatKyrgyz = "Президенте"
    }
},
bgcolor = "#ff0000",
fgcolor = "#ffff00"
}
```
Next you need a file with the magic `|%` and `%|` symbols.
```html
<title>Famous Celebreties of |%Country%|</title>
<style type = "text/css">
    body {
        background-color: |%bgcolor%|; color: |%fgcolor%|;
    }
</style>
|%:celebblock ipairs FamousPeople i, celeb %|
<!-- everything in here is done for each of the celebs -->
<h1>|%celeb.name%|</h1>
<h2>|%celeb.WhatKyrgyz%| (|%celeb.what%|)</h2>
<table>
|%:songblock pairs celeb.MyFavoriteSongs st, surl%|
    <tr><a href="|%surl%|">|%st%|</a></tr>
|%:songblock%|
</table>
|%:celebblock%|
```
Now, we load our file somehow.
I will assume a `dumpfile` function that loads a file into a string
```lua
filestr, err = dumpfile("sample.jtl.html")
if err then
    print(err)
else
    print(jtl.fill(filestr,values))
end
```

And you are done!
Whoa, I am going to bed, so I can't explain it more now, but perhaps I will edit it and explain more later.

# Why are their so many low quality bits of code?
https://librivox.org/the-brothers-karamazov-by-fyodor-dostoyevsky/
https://invidious.kavin.rocks/watch?v=3Cf7GxBzzHQ
https://www.super.kg/media/?search_author=683&media_search=1
